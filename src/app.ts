const form = document.querySelector<HTMLFormElement>("form")!;
const ageInput = document.querySelector<HTMLInputElement>("#age")!;
const themesInput = document.querySelector<HTMLInputElement>("#themes")!;
const submitButton = document.querySelector<HTMLButtonElement>("button")!;
const footer = document.querySelector<HTMLElement>("footer")!;

const OPENAI_API_KEY = "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";


const generatePromptByAgeAndThemes = (age: number, themes = "") => {
    let prompt = `Give me, in a happy and friendly tone, 5 gift ideas for a ${age} year old.`;

    if (themes.trim()) {
        prompt += `and who loves ${themes}`;
    }

    return prompt + " !";
}

/**
 * Put the button and the footer in "loading" mode
 */
const setLoadingItems = () => {
    footer.textContent = "Loading great ideas in progress!";
    footer.setAttribute("aria-busy", "true");
    submitButton.disabled = true;
}

/**
 * Remove button and footer "loading" mode
 */
const removeLoadingItems = () => {
    footer.setAttribute("aria-busy", "false");
    submitButton.setAttribute("aria-busy", "false");
    submitButton.disabled = false;
}

/**
 * Launch the whole system when the form is submitted
 */
form.addEventListener("submit", (e: SubmitEvent) => {
    // Cancel page reload
    e.preventDefault();

    // Put the footer and the button in "loading" mode
    callOpenaiApi();
})

/**
 * Translate text to HTML
 */
const translateTextToHtml = (text: string) => {
    return text
        .split("\n")
        .map((str) => `<p>${str}</p>`)
        .join("");
}

/**
 * Call the API passing it the question
 */

const callOpenaiApi = () => {
    setLoadingItems();
    fetch(`https://api.openai.com/v1/completions`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${OPENAI_API_KEY}`
        },
         body: JSON.stringify({
            prompt: generatePromptByAgeAndThemes(
                ageInput.valueAsNumber, 
                themesInput.value
            ),
            max_tokens: 2000,
            model: "text-davinci-003"
         }),
    })
    .then((response) => response.json())
    .then((data) => {
        footer.innerHTML = translateTextToHtml(data.choices[0].text);
    })
    .finally(() => removeLoadingItems())
}



